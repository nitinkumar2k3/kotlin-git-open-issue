package com.nits.gitissuesapp

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog

class Util {
    companion object{
        val BASE_URL = "https://api.github.com/repos/firebase/firebase-ios-sdk/"

        fun showAlertDialog(messg: String, context: Activity){
            val dialogBuilder = AlertDialog.Builder(context)
            dialogBuilder.setMessage(messg)
                .setCancelable(false)
                .setPositiveButton("", DialogInterface.OnClickListener {
                        dialog, id -> context.finish()
                })
                .setNegativeButton("Okay!", DialogInterface.OnClickListener {
                        dialog, id -> dialog.cancel()
                    context.finish()
                })

            val alert = dialogBuilder.create()
            alert.setTitle("Alert")
            alert.show()
        }
    }

}