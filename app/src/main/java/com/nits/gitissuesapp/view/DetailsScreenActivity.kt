package com.nits.gitissuesapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.nits.gitissuesapp.adapter.CommentItemAdapter
import com.nits.gitissuesapp.network.FragmentVMFactory
import com.nits.gitissuesapp.R
import com.nits.gitissuesapp.Util
import com.nits.gitissuesapp.databinding.ActivityDetailsScreenBinding
import com.nits.gitissuesapp.model.CommentsDataModel
import com.nits.gitissuesapp.network.Connectivity
import com.nits.gitissuesapp.network.RemoteRepository
import com.nits.gitissuesapp.viewmodel.CommentsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_details_screen.*
import kotlinx.android.synthetic.main.activity_details_screen.loader_view
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.collections.ArrayList


class DetailsScreenActivity : AppCompatActivity() {
    var compositeDisposable = CompositeDisposable()
    lateinit var binding: ActivityDetailsScreenBinding
    var context: Context? = null
    lateinit var commentsViewModel: CommentsViewModel
    lateinit var commentsDataList: ArrayList<CommentsDataModel>
    var issueNumber: String = ""

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindUI()

        if (!Connectivity.isConnected(context)){
            Util.showAlertDialog("No internet connection! Please check wifi / data", this)
            return
        }

        commentsViewModel = ViewModelProviders.of(this, FragmentVMFactory(RemoteRepository.getInstance(), application))
                .get(CommentsViewModel::class.java)

        if (!TextUtils.isEmpty(intent.getStringExtra("issueNum"))){
            issueNumber = intent.getStringExtra("issueNum")
        }

        loader_view.visibility = View.VISIBLE
        commentsViewModel.fetchCommentsByNumber(issueNumber)
        commentsViewModel.getAllCommentsList().observe(this, Observer {
            compositeDisposable.add(it
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {result ->
                        commentsDataList = ArrayList<CommentsDataModel>(result)

                        if (commentsDataList.size > 0) {
                            comment_recylerview?.let {
                                comment_recylerview.layoutManager = LinearLayoutManager(this)
                                comment_recylerview.adapter = CommentItemAdapter(commentsDataList, this)
                            }
                            showCommentUI(commentsDataList)
                        }else{
                           showNoCommentUI()
                        }
                        loader_view.visibility = View.GONE
                    },
                    {
                        loader_view.visibility = View.GONE
                        Util.showAlertDialog(it.message.toString(), this)
                    }
                ))
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun bindUI(){
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details_screen)
        context = this
    }

    private fun showNoCommentUI(){
        comment_recylerview.visibility = View.GONE
        no_commnet_lbl.visibility = View.VISIBLE
        Util.showAlertDialog("Comments not available", this)
    }

    private fun showCommentUI(commentsDataList: ArrayList<CommentsDataModel>){
        no_commnet_lbl.visibility = View.GONE
        Toast.makeText(this, commentsDataList.size.toString()+" comment found!", Toast.LENGTH_SHORT).show()
    }

}
