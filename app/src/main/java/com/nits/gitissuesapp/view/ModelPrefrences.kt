package com.nits.gitissuesapp.view

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nits.gitissuesapp.model.GitIssuesDataModel
import java.lang.reflect.Type


class ModelPreferences(context: Context) {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences("MODEL_PREFERENCES", Context.MODE_PRIVATE)

    private val editor = sharedPreferences.edit()

    fun deleteOldData(){
        editor.clear()
        editor.commit()
    }

    fun <T> setList(key: String?, list: List<T>?) {
        val gson = Gson()
        val json = gson.toJson(list)
        set(key, json)
    }

    operator fun set(key: String?, value: String?) {
        editor.putString(key, value)
        editor.commit()
    }

    fun checkIfDataExist(key: String): Boolean{
        if (sharedPreferences.contains(key)){
            return true
        }else{
            return false
        }
    }

    fun getList(key: String?): ArrayList<GitIssuesDataModel> {
        var gitIssue: ArrayList<GitIssuesDataModel> = ArrayList()
        val gson = Gson()

        val jsonPreferences = sharedPreferences.getString(key, "")

        val type: Type = object : TypeToken<ArrayList<GitIssuesDataModel>>() {}.type
        gitIssue = gson.fromJson(jsonPreferences, type)

        return gitIssue
    }
}