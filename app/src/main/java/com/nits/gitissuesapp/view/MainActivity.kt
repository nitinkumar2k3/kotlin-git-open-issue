package com.nits.gitissuesapp.view

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.nits.gitissuesapp.R
import com.nits.gitissuesapp.Util
import com.nits.gitissuesapp.adapter.ItemAdapter
import com.nits.gitissuesapp.databinding.ActivityMainBinding
import com.nits.gitissuesapp.model.GitIssuesDataModel
import com.nits.gitissuesapp.network.Connectivity
import com.nits.gitissuesapp.network.FragmentVMFactory
import com.nits.gitissuesapp.network.RemoteRepository
import com.nits.gitissuesapp.viewmodel.GitIssueViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.reflect.Type
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    var compositeDisposable = CompositeDisposable()
    lateinit var binding: ActivityMainBinding
    lateinit var gitIssuesViewModel: GitIssueViewModel
    var context: Context? = null
    lateinit var gitIssuesDataList: ArrayList<GitIssuesDataModel>

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindUI()
        gitIssuesViewModel =
            ViewModelProviders.of(
                this,
                FragmentVMFactory(RemoteRepository.getInstance(), application)
            )
                .get(GitIssueViewModel::class.java)

        if (ModelPreferences(this).checkIfDataExist("issues")) {
            val data = ModelPreferences(this).getList("issues")
            var shareDataList = data
            gitIssuesDataList = shareDataList
            sortAndShowData(gitIssuesDataList)
        } else {
            if (Connectivity.isConnected(context)) {
                callAPiAndFetch()
            } else {
                Util.showAlertDialog("No internet connection! Please check wifi / data", this)
            }
        }

    }

    fun callAPiAndFetch() {
        loader_view.visibility = View.VISIBLE
        gitIssuesViewModel.fetchIssuesData()
        gitIssuesViewModel.getAllFetchedIssuesList().observe(this, Observer {
            compositeDisposable.add(
                it.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { result ->
                            gitIssuesDataList = ArrayList<GitIssuesDataModel>(result)
                            if (gitIssuesDataList.size > 0) {
                                issues_recylerview?.let {

                                    ModelPreferences(this).deleteOldData()
                                    ModelPreferences(this).setList("issues", gitIssuesDataList)
                                    val data = ModelPreferences(this).getList("issues")
                                    sortAndShowData(data)
                                    Log.i("__shared data", data.size.toString())
                                }
                                Toast.makeText(
                                    this,
                                    gitIssuesDataList.size.toString() + " issue(s) found!",
                                    Toast.LENGTH_SHORT
                                ).show()

                            } else {
                                Toast.makeText(this, "No issues found!", Toast.LENGTH_SHORT).show()
                            }
                            Log.i("result", gitIssuesDataList.size.toString())
                            loader_view.visibility = View.GONE
                        },

                        { it ->
                            loader_view.visibility = View.GONE
                            Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                        })
            )
        })
    }

    private fun sortAndShowData(gitIssuesDataList: ArrayList<GitIssuesDataModel>) {
        Collections.sort(
            gitIssuesDataList,
            object : Comparator<GitIssuesDataModel> {
                var f: DateFormat =
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)

                override fun compare(
                    p0: GitIssuesDataModel?, p1: GitIssuesDataModel?
                ): Int {
                    return f.parse(p1!!.updatedAt).compareTo(f.parse(p0!!.updatedAt))
                }
            })
        issues_recylerview.layoutManager = LinearLayoutManager(this)
        issues_recylerview.adapter = ItemAdapter(gitIssuesDataList, this)

    }

    private fun bindUI() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        context = this
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

}
