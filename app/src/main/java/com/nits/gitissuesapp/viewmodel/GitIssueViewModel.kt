package com.nits.gitissuesapp.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nits.gitissuesapp.model.GitIssuesDataModel
import com.nits.gitissuesapp.network.RemoteRepository
import io.reactivex.Single
import kotlinx.coroutines.launch

class GitIssueViewModel(private val remoteRepository: RemoteRepository, val application: Application): ViewModel() {
    private val issuesData = MutableLiveData<Single<List<GitIssuesDataModel>>>()

    fun fetchIssuesData(){
        issuesData.postValue(remoteRepository.getAllIssuesData())
    }

    fun getAllFetchedIssuesList() = issuesData as LiveData<Single<List<GitIssuesDataModel>>>

//    lateinit var dbRepos: DbRepos
//
//    init {
//        // Gets reference to WordDao from WordRoomDatabase to construct
//        // the correct WordRepository.
//        val gitDao = GitRoomDatabase.getDatabase(application).gitdao()
//        //DbRepos = DbRepos(gitDao)
//
//    }

//    fun insert(commentList: GitIssuesDataModel) = viewModelScope.launch {
//        dbRepos.insertCommentList(commentList)
//    }
}