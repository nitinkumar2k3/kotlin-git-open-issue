package com.nits.gitissuesapp.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nits.gitissuesapp.model.CommentsDataModel
import com.nits.gitissuesapp.network.RemoteRepository
import io.reactivex.Single

class CommentsViewModel(private val remoteRepository: RemoteRepository,  val application: Application): ViewModel() {
    private val commentsData = MutableLiveData<Single<List<CommentsDataModel>>>()

    fun fetchCommentsByNumber(issueNum: String){
        commentsData.postValue(remoteRepository.getAllCommentsList(issueNum))
    }

    fun getAllCommentsList() = commentsData as LiveData<Single<List<CommentsDataModel>>>

}