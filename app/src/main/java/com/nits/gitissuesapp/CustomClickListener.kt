package com.nits.gitissuesapp

import com.nits.gitissuesapp.model.GitIssuesDataModel

interface CustomClickListener {
   public fun itemClicked(gitIssuesDataModel: GitIssuesDataModel)
}