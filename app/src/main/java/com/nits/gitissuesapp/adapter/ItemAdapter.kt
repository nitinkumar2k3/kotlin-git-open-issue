package com.nits.gitissuesapp.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.nits.gitissuesapp.CustomClickListener
import com.nits.gitissuesapp.databinding.ItemRowBinding
import com.nits.gitissuesapp.model.GitIssuesDataModel
import com.nits.gitissuesapp.view.DetailsScreenActivity


class ItemAdapter(var gitIssuesDataModel: ArrayList<GitIssuesDataModel>, val context: Context)  :
    RecyclerView.Adapter<ItemAdapter.GitViewHolder>(),
    CustomClickListener {

    override fun getItemCount(): Int {
       return gitIssuesDataModel.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRowBinding.inflate(inflater)
        return GitViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GitViewHolder, position: Int) {
        holder.bind(gitIssuesDataModel[position])
        holder.view.itemClickListener = this

    }

    inner class GitViewHolder(val view: ItemRowBinding) : RecyclerView.ViewHolder(view.root) {
        fun bind(gitIssuesDataModel: GitIssuesDataModel){
            with(view){
                issueItemCard.setOnClickListener { itemClickListener?.itemClicked(gitIssuesDataModel) }
                titleTxt.text = gitIssuesDataModel.title.toString()
                descriptionTxt.text = gitIssuesDataModel.body.substring(0, Math.min(gitIssuesDataModel.body.length, 140));
            }
        }
    }

    override fun itemClicked(gitIssuesDataModel: GitIssuesDataModel) {
        val intent = Intent(context, DetailsScreenActivity::class.java)
        intent.putExtra("issueNum", gitIssuesDataModel.number.toString())
        context.startActivity(intent)
    }

}