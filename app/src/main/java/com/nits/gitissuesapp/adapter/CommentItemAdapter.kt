package com.nits.gitissuesapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nits.gitissuesapp.databinding.CommentItemRowBinding
import com.nits.gitissuesapp.model.CommentsDataModel


class CommentItemAdapter(var commentsDataModel: ArrayList<CommentsDataModel>, val context: Context)  :
    RecyclerView.Adapter<CommentItemAdapter.GitViewHolder>() {

    override fun getItemCount(): Int {
       return commentsDataModel.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CommentItemRowBinding.inflate(inflater)
        return GitViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GitViewHolder, position: Int) {
        holder.bind(commentsDataModel[position])
    }

    inner class GitViewHolder(val view: CommentItemRowBinding) : RecyclerView.ViewHolder(view.root) {
        fun bind(commentsDataModel: CommentsDataModel){
            with(view){

                usernameTxt.text = "Username: "+commentsDataModel.user.login.toString()
                commentTxt.text = commentsDataModel.body.toString()
            }
        }
    }

}