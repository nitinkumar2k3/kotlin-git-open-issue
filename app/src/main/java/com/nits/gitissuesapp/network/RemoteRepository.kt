package com.nits.gitissuesapp.network

import com.mirafra.ideation.service.ApiInterface
import com.mirafra.ideation.service.retrofitServiceObj
import com.nits.gitissuesapp.model.CommentsDataModel
import com.nits.gitissuesapp.model.GitIssuesDataModel
import io.reactivex.Single

class RemoteRepository {

    var client: ApiInterface = retrofitServiceObj
    companion object{
        @Volatile private var  instance: RemoteRepository ?= null

        fun getInstance()=
            instance?: synchronized(this){
                instance ?: RemoteRepository().also { instance = it }
            }
    }

    fun getAllIssuesData(): Single<List<GitIssuesDataModel>>{

        return client.getAllIssuesList()
    }

    fun getAllCommentsList(issueNum: String): Single<List<CommentsDataModel>>{

        return client.getAllCommentsList(issueNum)
    }
}