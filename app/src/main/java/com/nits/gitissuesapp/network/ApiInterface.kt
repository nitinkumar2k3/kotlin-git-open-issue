package com.mirafra.ideation.service

import com.nits.gitissuesapp.model.CommentsDataModel
import com.nits.gitissuesapp.model.GitIssuesDataModel
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*
import java.util.*

/*
A suspending function is simply a function that can be paused and resumed at a later time.
They can execute a long running operation and wait for it to complete without blocking
 */
interface ApiInterface {
    @GET("issues")
    fun getAllIssuesList():Single<List<GitIssuesDataModel>>

    @GET("issues/{issuenum}/comments")
    fun getAllCommentsList(@Path("issuenum") issuenum: String): Single<List<CommentsDataModel>>
}