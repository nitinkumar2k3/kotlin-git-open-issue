package com.nits.gitissuesapp.network

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nits.gitissuesapp.viewmodel.CommentsViewModel
import com.nits.gitissuesapp.viewmodel.GitIssueViewModel


class FragmentVMFactory(private val remoteRepo: RemoteRepository, val application: Application) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GitIssueViewModel::class.java)) {
            return GitIssueViewModel(remoteRepo, application) as T
        } else if (modelClass.isAssignableFrom(CommentsViewModel::class.java)) {
            return CommentsViewModel(remoteRepo, application) as T
        }
        throw IllegalArgumentException("Unsupported Model class")
    }
}