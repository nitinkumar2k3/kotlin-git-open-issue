# README #

Android kotlin sample app for Implement persistent storage 
in the application for caching data so that the issues are only fetched and displayed.

Documentation for the GitHub issues API can be found here: http://developer.github.com/v3/issues

The URL to fetch issues associated with the firebase-ios-sdkrepository can be found here: https://api.github.com/repos/firebase/firebase-ios-sdk/issues

Comments of an Issue: https://api.github.com/repos/firebase/firebase-ios-sdk/issues/3228/comments

